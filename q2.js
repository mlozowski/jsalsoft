/**
ES3
*/

function House(width, length, height) {
    this.width = width;
    this.length = length;
    this.height = height;
}

House.prototype.isSmall = function () {
    return (this.width * this.length * this.heigh) < 100;
};

// Brick
function BrickHouse(width, length, height) {
	this.width = width;
	this.length = length;
	this.height = height;
}

BrickHouse.prototype.isSmall = House.prototype.isSmall;

BrickHouse.prototype.sayHello = function () {
	alert("Hello!");
};

// Wooden
function WoodenHouse(width, length, height) {
	this.width = width;
	this.length = length;
	this.height = height;
}

WoodenHouse.prototype.isSmall = House.prototype.isSmall;

// Straw
function StrawHouse(width, length, height) {
	this.width = width;
	this.length = length;
	this.height = height;
}

StrawHouse.prototype.isSmall = House.prototype.isSmall;

