/**
ES6
*/

class House {

  constructor(width, length, height) {
  	this.width = width;
    this.length = length;
    this.height = height;
  }

  get isSmall() {
    return (this.width * this.length * this.heigh) < 100;
  }

}

class BrickHouse extends House {

  static get welcomeMessage() {
    return "Hello!";
  }

}

class WoodenHouse extends House {

  static get welcomeMessage() {
    return "Hello wood!";
  }

}

class StrawHouse extends House {

  static get welcomeMessage() {
    return "Hello straw!";
  }

}
